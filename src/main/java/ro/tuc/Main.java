package ro.tuc;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

//        Map<String, String> teacherToCoursesMap = new HashMap<>();
//        teacherToCoursesMap.put("John Doe", "Distributed Systems");
//        teacherToCoursesMap.put("Mary Jones", "Mathematics");
//        teacherToCoursesMap.put("Ann Smith", "Physics");
//        teacherToCoursesMap.put(null, "Smth");
//
//        System.out.println("The map is " + teacherToCoursesMap);
//
//        System.out.println("The List is: ");
//        for(Map.Entry<String, String> entry: teacherToCoursesMap.entrySet()){
//            System.out.println("Teacher=" + entry.getKey() + "; " +
//                    "Course=" + entry.getValue());
//        }
//
//        System.out.println(teacherToCoursesMap.replace("Ann Smith", "Physics", null));
//
//        System.out.println("Map after replacement " + teacherToCoursesMap);
//
//        System.out.println("Get value from map " + teacherToCoursesMap.get("John Doe"));
//
//        teacherToCoursesMap.remove("John Doe");
//
//        System.out.println("Map after remove " + teacherToCoursesMap);
//
//        teacherToCoursesMap.clear();
//
//        System.out.println("Map after clear" + teacherToCoursesMap);


        Person person1 = new Person("Bob", 30);
        Person person2 = new Person("Bob", 30);
        Person person3 = new Person("Alice", 20);
        Map<Person, String> myMap = new HashMap<>();


        System.out.println("Are person1 and person2 equal? " + person1.equals(person2));
        System.out.println("Hashcode for person1: " + person1.hashCode());
        System.out.println("Hashcode for person2: " + person2.hashCode());

        myMap.put(person1, "aab");
        System.out.println(myMap.get(person2));
    }
}